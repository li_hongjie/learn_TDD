import unittest

from app.calculator import Calculator

class TddInPythonExample(unittest.TestCase):
    def setUp(self):
        self.calc = Calculator()

    def test_calculator_add_method_return_correct_result(self):
        result = self.calc.add(2, 2)
        self.assertEqual(4, result)

    def test_calculator_return_error_message_if_bath_args_not_digit(self):
        self.assertRaises(ValueError, self.calc.add, 'two', 'three')

    def test_calculator_return_error_message_if_x_args_not_digit(self):
        self.assertRaises(ValueError, self.calc.add, 2, 'three')

    def test_calculator_return_error_message_if_y_args_not_digit(self):
       self.assertRaises(ValueError, self.calc.add, 'two', 3)
